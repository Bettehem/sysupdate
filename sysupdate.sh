#!/bin/bash
echo "Checking for updates..."
pacaur -Sy
echo "$(( $(checkupdates | wc -l) + $(pacaur -k | wc -l) )) updates available."
printf "Show available updates? [Y/n]: "
read SHOWUPDATES
if [[ $SHOWUPDATES == "y" || $SHOWUPDATES == "Y" || $SHOWUPDATES == "" ]]; then
	echo "Updates in Arch Repositories:"
	checkupdates
	echo "Updates in Arch User Repositories:"
	pacaur -k
elif [[ $SHOWUPDATES == "n" || $SHOWUPDATES == "N" ]]; then
	echo "Not showing list of updates."
fi

printf "Install updates? [Y/n]: "
read INSTALLUPDATES
if [[ $INSTALLUPDATES == "y" || $INSTALLUPDATES == "Y" || $INSTALLUPDATES == "" ]]; then
	pacaur -Syu
elif [[ $INSTALLUPDATES == "n" || $INSTALLUPDATES == "N" ]]; then
	echo "Not installing updates."
	exit
fi
